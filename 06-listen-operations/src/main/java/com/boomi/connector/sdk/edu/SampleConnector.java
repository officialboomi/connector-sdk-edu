// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.ConnectorContext;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.listen.ListenConnector;
import com.boomi.connector.api.listen.ListenOperation;
import com.boomi.connector.util.BaseConnector;

/**
 * 
 */
public class SampleConnector extends BaseConnector implements ListenConnector<PollingListenManager> {

    @Override
    public Browser createBrowser(BrowseContext context) {
        return new SampleBrowser(context);
    }

    @Override
    protected Operation createExecuteOperation(OperationContext context) {
        return new SampleExecuteOperation(context);
    }

    @Override
    protected Operation createGetOperation(OperationContext context) {
        return new SampleGetOperation(context);
    }

    @Override
    protected Operation createDeleteOperation(OperationContext context) {
        return new SampleDeleteOperation(context);
    }

    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new SampleQueryOperation(context);
    }

    @Override
    public ListenOperation<PollingListenManager> createListenOperation(OperationContext context) {
        return new PollingListenOperation(context);
    }

    @Override
    public PollingListenManager createListenManager(ConnectorContext context) {
        return new PollingListenManager(context);
    }
 
    
}

// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import java.util.logging.Logger;

import com.boomi.connector.api.listen.Listener;
import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.Payload;
import com.boomi.connector.util.listen.BasePollingOperation;
import com.boomi.util.CollectionUtil;
import com.boomi.util.CollectionUtil.Function;
import com.boomi.util.LogUtil;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * 
 */
public class PollingListenOperation extends BasePollingOperation<PollingListenManager> {

    private static final Logger LOG = LogUtil.getLogger(PollingListenOperation.class);
    private static final Function<JsonNode, Payload> TO_PAYLOAD = new Function<JsonNode, Payload>() {
        @Override
        public Payload apply(JsonNode json) {
            return JsonPayloadUtil.toPayload(json);
        }
    };
    
    protected PollingListenOperation(OperationContext context) {
        super(context);
    }

    @Override
    protected void doStart(Listener listener, PollingListenManager manager) {
        LOG.info("starting");
    }

    @Override
    protected void doStop() {
        LOG.info("stopping");
    }

    @Override
    protected Iterable<Payload> doPoll() {
        return CollectionUtil.apply(People.queryByFirstName("charlie"), TO_PAYLOAD);
    }

}

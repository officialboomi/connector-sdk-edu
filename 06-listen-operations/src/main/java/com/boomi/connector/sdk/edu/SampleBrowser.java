// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import java.io.IOException;
import java.util.Collection;
import java.util.EnumSet;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.util.BaseBrowser;
import com.boomi.util.json.JSONUtil;

/**
 * 
 */
public class SampleBrowser extends BaseBrowser {

    private static final long DEFAULT_OBJECT_COUNT = 10L;

    protected SampleBrowser(BrowseContext context) {
        super(context);
    }

    @Override
    public ObjectTypes getObjectTypes() {
        ObjectTypes types = new ObjectTypes();
        long objectCount = getContext().getOperationProperties().getLongProperty("objectCount", DEFAULT_OBJECT_COUNT);
        for (int i = 1; i <= objectCount; i++) {
            types.withTypes(new ObjectType().withId("object" + i).withLabel("Object Type " + i));
        }
        return types;
    }

    @Override
    public ObjectDefinitions getObjectDefinitions(String objectTypeId, Collection<ObjectDefinitionRole> roles) {
        ObjectDefinitions definitions = new ObjectDefinitions();
        for (ObjectDefinitionRole role : roles) {
            definitions.withDefinitions(getDefinition(role));
        }
        return definitions;
    }

    private ObjectDefinition getDefinition(ObjectDefinitionRole role) {
        if (EnumSet.of(OperationType.QUERY, OperationType.LISTEN).contains(getContext().getOperationType())) {
            return getJsonDefinition(role, "person.json");
        }
        return new ObjectDefinition().withInputType(getInputType(role)).withOutputType(getOutputType(role));
    }

    private static ObjectDefinition getJsonDefinition(ObjectDefinitionRole role, String resourceName) {
        try {
            return JSONUtil.newJsonDefinitionFromResource(role, resourceName);
        } catch (IOException e) {
            throw new ConnectorException(e);
        }
    }

    private static ContentType getInputType(ObjectDefinitionRole role) {
        return (role == ObjectDefinitionRole.INPUT) ? ContentType.BINARY : ContentType.NONE;
    }

    private ContentType getOutputType(ObjectDefinitionRole role) {
        if ((role == ObjectDefinitionRole.OUTPUT) && (getContext().getOperationType() != OperationType.DELETE)) {
            return ContentType.BINARY;
        }
        return ContentType.NONE;
    }

}

// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.QueryRequest;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.connector.util.BaseQueryOperation;
import com.boomi.util.Args;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * 
 */
public class SampleQueryOperation extends BaseQueryOperation {

    protected SampleQueryOperation(OperationContext context) {
        super(context);
    }

    @Override
    protected void executeQuery(QueryRequest request, OperationResponse response) {
        FilterData input = request.getFilter();
        QueryFilter filter = input.getFilter();

        SimpleExpression simpleExpression = Args.instanceOf(filter.getExpression(), SimpleExpression.class);

        String property = simpleExpression.getProperty();
        String argument = simpleExpression.getArguments().get(0);
        List<Sort> sortFields = filter.getSort();
        List<String> selectedFields = getContext().getSelectedFields();

        try {
            Iterable<JsonNode> people = query(property, argument, selectedFields, sortFields);
            for (JsonNode person : people) {
                response.addPartialResult(input, OperationStatus.SUCCESS, "200", "good person",
                        JsonPayloadUtil.toPayload(person));
            }
        } catch (Exception e) {
            input.getLogger().log(Level.SEVERE, "failed query", e);
            response.addPartialResult(input, OperationStatus.APPLICATION_ERROR, "400", "bad person", null);
        }
        response.finishPartialResult(input);

    }

    private static Iterable<JsonNode> query(String property, String argument, List<String> selectedFields,
            List<Sort> sortFields) {
        if ("firstName".equals(property)) {
            return People.queryByFirstName(argument, selectedFields, sortFields);
        }
        if ("age".equals(property)) {
            return People.queryByAge(Integer.parseInt(argument), selectedFields, sortFields);
        }
        return Collections.emptyList();
    }

}

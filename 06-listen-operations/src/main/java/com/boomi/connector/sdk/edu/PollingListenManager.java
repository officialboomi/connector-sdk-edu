// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import java.util.logging.Logger;

import com.boomi.connector.api.ConnectorContext;
import com.boomi.connector.util.listen.BasePollingManager;
import com.boomi.util.LogUtil;

/**
 * 
 */
public class PollingListenManager extends BasePollingManager {

    private static final Logger LOG = LogUtil.getLogger(PollingListenManager.class);
    
    
    protected PollingListenManager(ConnectorContext context) {
        super(context);
    }

    @Override
    protected void doStart() {
        LOG.info("starting");
    }

    @Override
    protected void doStop() {
        LOG.info("stopping");
    }

}

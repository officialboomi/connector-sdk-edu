// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.boomi.connector.api.Sort;
import com.boomi.util.CollectionUtil;
import com.boomi.util.StringUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Helper class to simulate various queries against a "people" api
 */
public final class People {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private People() {

    }

    /**
     * Returns 10 "person" objects with the provided last name to demonstrate partial results.
     */
    public static Iterable<JsonNode> queryByFirstName(String value) {
        return queryByFirstName(value, null, null);
    }

    /**
     * Returns 10 "person" objects with the provided last name to demonstrate selection and sorting of partial results.
     */
    public static Iterable<JsonNode> queryByFirstName(String value, List<String> selectedFields,
            List<Sort> sortFields) {
        if (StringUtil.isBlank(value)) {
            return Collections.emptyList();
        }
        List<JsonNode> people = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            people.add(createPerson(value, "last" + i, i, selectedFields));
        }
        Collections.sort(people, new PersonComparator(sortFields));
        return people;
    }

    /**
     * Returns two "person" objects with the provided age. In order to demonstrate partial failures, iterating past the
     * first two results will cause a "query failure"
     */
    public static Iterable<JsonNode> queryByAge(int age) {
        return queryByAge(age, null, null);
    }

    /**
     * Returns two "person" objects with the provided age. In order to demonstrate partial failures with selection and
     * sorting, iterating past the first two results will cause a "query failure"
     */
    public static Iterable<JsonNode> queryByAge(int age, List<String> selectedFields, List<Sort> sortFields) {
        List<JsonNode> people = new ArrayList<>();
        people.add(createPerson("charlie", "kelly", age, selectedFields));
        people.add(createPerson("frank", "reynolds", age, selectedFields));
        Collections.sort(people, new PersonComparator(sortFields));
        return new BadIterable<>(people);
    }

    private static JsonNode createPerson(String firstName, String lastName, int age, List<String> selectedFields) {
        Map<String, Object> map = new HashMap<>();
        putIfSelected(map, "firstName", firstName, selectedFields);
        putIfSelected(map, "lastName", lastName, selectedFields);
        putIfSelected(map, "age", age, selectedFields);
        return MAPPER.valueToTree(map);
    }

    private static void putIfSelected(Map<String, Object> map, String field, Object value,
            List<String> selectedFields) {
        if ((CollectionUtil.isEmpty(selectedFields)) || (selectedFields.contains(field))) {
            map.put(field, value);
        }
    }

    private static class PersonComparator implements Comparator<JsonNode> {

        private final List<Sort> _sortFields;

        public PersonComparator(List<Sort> sortFields) {
            _sortFields = sortFields;
        }

        @Override
        public int compare(JsonNode n1, JsonNode n2) {
            if (CollectionUtil.isEmpty(_sortFields)) {
                return 0;
            }
            for (Sort sort : _sortFields) {
                String field = sort.getProperty();
                int result = n1.path(field).asText().compareTo(n2.path(field).asText());
                if (result != 0) {
                    return ("descending".equals(sort.getSortOrder())) ? (result * -1) : result;
                }
            }
            return 0;
        }
    }

    private static class BadIterable<T> implements Iterable<T> {

        private final Iterator<T> _iter;

        public BadIterable(Iterable<T> iterable) {
            _iter = iterable.iterator();
        }

        @Override
        public Iterator<T> iterator() {
            return new Iterator<T>() {

                @Override
                public boolean hasNext() {
                    if (!_iter.hasNext()) {
                        throw new RuntimeException("query failure");
                    }
                    return true;
                }

                @Override
                public T next() {
                    return _iter.next();
                }

            };
        }
    }

}

// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;

/**
 * 
 */
public class CombinedResultOperation extends BaseUpdateOperation {

    protected CombinedResultOperation(OperationContext context) {
        super(context);
    }

    @Override
    protected void executeUpdate(UpdateRequest request, OperationResponse response) {
        response.addCombinedResult(request, OperationStatus.SUCCESS, "200", "combined!", null);
    }
    
}

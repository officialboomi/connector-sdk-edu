// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import com.boomi.connector.api.ExtendedPayload;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PayloadMetadata;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 */
public class SampleExecuteOperation extends BaseUpdateOperation {

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final String INPUT_PROP_ID = "inputProp";
    private static final String OUTPUT_PROP_ID = "outputProp";
    
    private final String _connectionId; 
    private final String _operationId; 
    private final boolean _includePayload; 
    
    protected SampleExecuteOperation(OperationContext context) {
        super(context);
        _connectionId = context.getConnectionProperties().getProperty("id");
        _operationId = context.getOperationProperties().getProperty("id");
        _includePayload = context.getOperationProperties().getBooleanProperty("includePayload", false);
    }

    @Override
    protected void executeUpdate(UpdateRequest request, OperationResponse response) {
        response.getLogger().info("connection id: " + _connectionId);
        for (ObjectData input : request) {
            input.getLogger().info("operation id: " + _operationId);
            input.getLogger().info(INPUT_PROP_ID + ": " + input.getDynamicProperties().get(INPUT_PROP_ID));
            Payload payload = (_includePayload ? createPayload(input) : null);
            response.addResult(input, OperationStatus.SUCCESS, "200", "great succues!", payload);
        }
    }

    private Payload createPayload(final TrackedData input) {
        final PayloadMetadata metadata = getContext().createMetadata();
        metadata.setTrackedProperty(OUTPUT_PROP_ID, new Date().toString());
        
        return new ExtendedPayload() {
            
            @Override
            public void close() throws IOException {
                //do nothing 
            }
            
            @Override
            public void writeTo(OutputStream out) throws IOException {
                MAPPER.writeValue(out, input.getUserDefinedProperties());
            }
            
            @Override
            public InputStream readFrom() throws IOException {
                return null;
            }
            
            @Override
            public PayloadMetadata getMetadata() {
                return metadata;
            }
            
        };
       
    }

}

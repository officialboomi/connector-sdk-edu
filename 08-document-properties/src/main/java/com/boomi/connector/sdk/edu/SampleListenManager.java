// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import java.util.logging.Logger;

import com.boomi.connector.api.ConnectorContext;
import com.boomi.connector.util.listen.BaseListenManager;
import com.boomi.util.LogUtil;

/**
 * 
 */
public class SampleListenManager extends BaseListenManager {

    private static final Logger LOG = LogUtil.getLogger(SampleListenManager.class);
    
    protected SampleListenManager(ConnectorContext context) {
        super(context);
    }

    @Override
    public void start() {
        LOG.info("starting");
    }

    @Override
    public void stop() {
        LOG.info("stopping");
    }

}

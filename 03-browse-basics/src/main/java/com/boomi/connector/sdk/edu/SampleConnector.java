// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.BaseConnector;

/**
 * 
 */
public class SampleConnector extends BaseConnector {

    @Override
    public Browser createBrowser(BrowseContext context) {
        return new SampleBrowser(context);
    }

    @Override
    protected Operation createExecuteOperation(OperationContext context) {
        return new SampleExecuteOperation(context);
    }
    
}

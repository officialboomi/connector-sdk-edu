// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import java.util.Collection;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.util.BaseBrowser;

/**
 * 
 */
public class SampleBrowser extends BaseBrowser {

    private static final long DEFAULT_OBJECT_COUNT = 10L;

    protected SampleBrowser(BrowseContext context) {
        super(context);
    }

    @Override
    public ObjectTypes getObjectTypes() {
        ObjectTypes types = new ObjectTypes();
        long objectCount = getContext().getOperationProperties().getLongProperty("objectCount", DEFAULT_OBJECT_COUNT);
        for (int i = 1; i <= objectCount; i++) {
            types.withTypes(new ObjectType().withId("object" + i).withLabel("Object Type " + i));
        }
        return types;
    }

    @Override
    public ObjectDefinitions getObjectDefinitions(String objectTypeId, Collection<ObjectDefinitionRole> roles) {
        ObjectDefinitions definitions = new ObjectDefinitions();
        for (ObjectDefinitionRole role : roles) {
            definitions.withDefinitions(new ObjectDefinition()
                    .withInputType((role == ObjectDefinitionRole.INPUT) ? ContentType.BINARY : ContentType.NONE)
                    .withOutputType((role == ObjectDefinitionRole.OUTPUT) ? ContentType.BINARY : ContentType.NONE));
        }
        return definitions;
    }

}

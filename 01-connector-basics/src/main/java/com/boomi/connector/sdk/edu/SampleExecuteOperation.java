// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationRequest;
import com.boomi.connector.api.OperationResponse;

/**
 * 
 */
public class SampleExecuteOperation implements Operation {

    @Override
    public void execute(OperationRequest request, OperationResponse response) {
        throw new ConnectorException("bad things! have a nice day!");
    }

}

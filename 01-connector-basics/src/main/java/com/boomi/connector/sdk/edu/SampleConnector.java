// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import java.util.logging.Logger;

import com.boomi.connector.api.AtomContext;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.util.LogUtil;

/**
 * 
 */
public class SampleConnector implements Connector {
    
    private static final Logger LOG = LogUtil.getLogger(SampleConnector.class);

    @Override
    public void initialize(AtomContext context) {
        // do nothing 
    }

    @Override
    public Browser createBrowser(BrowseContext context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Operation createOperation(OperationContext context) {
        LOG.info("creating operation");
        return new SampleExecuteOperation();
    }

}

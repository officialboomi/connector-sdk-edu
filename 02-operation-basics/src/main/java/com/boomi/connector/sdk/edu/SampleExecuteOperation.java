// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import java.util.Date;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;

/**
 * 
 */
public class SampleExecuteOperation extends BaseUpdateOperation {

    private final String _connectionId; 
    private final String _operationId; 
    private final boolean _includePayload; 
    
    protected SampleExecuteOperation(OperationContext context) {
        super(context);
        _connectionId = context.getConnectionProperties().getProperty("id");
        _operationId = context.getOperationProperties().getProperty("id");
        _includePayload = context.getOperationProperties().getBooleanProperty("includePayload", false);
    }

    @Override
    protected void executeUpdate(UpdateRequest request, OperationResponse response) {
        response.getLogger().info("connection id: " + _connectionId);
        for (ObjectData input : request) {
            input.getLogger().info("operation id: " + _operationId);
            Payload payload = (_includePayload ? PayloadUtil.toPayload(new Date().toString()) : null);
            response.addResult(input, OperationStatus.SUCCESS, "200", "great succues!", payload);
        }
    }

}

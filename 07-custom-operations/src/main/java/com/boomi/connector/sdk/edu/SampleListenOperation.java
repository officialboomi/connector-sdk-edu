// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import java.util.logging.Logger;

import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.listen.Listener;
import com.boomi.connector.api.listen.PayloadBatch;
import com.boomi.connector.util.listen.BaseListenOperation;
import com.boomi.util.LogUtil;

/**
 * 
 */
public class SampleListenOperation extends BaseListenOperation<SampleListenManager> {
    
    private static Logger LOG = LogUtil.getLogger(SampleListenOperation.class);
    
    protected SampleListenOperation(OperationContext context) {
        super(context);
    }

    @Override
    public void start(Listener listener, SampleListenManager manager) {
        LOG.info("starting");
        PayloadBatch batch = listener.getBatch();  
        batch.add(PayloadUtil.toPayload("payload1"));
        batch.add(PayloadUtil.toPayload("payload2"));
        batch.add(PayloadUtil.toPayload("payload3"));
        batch.submit();
    }

    @Override
    public void stop() {
        LOG.info("stopping");
    }
    
}

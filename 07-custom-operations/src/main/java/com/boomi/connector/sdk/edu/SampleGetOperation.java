// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import com.boomi.connector.api.GetRequest;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.util.BaseGetOperation;

/**
 * 
 */
public class SampleGetOperation extends BaseGetOperation {

    protected SampleGetOperation(OperationContext context) {
        super(context);
    }

    @Override
    protected void executeGet(GetRequest request, OperationResponse response) {
        response.getLogger().info("Shared Field: " + getContext().getOperationProperties().getProperty("shared"));

        ObjectIdData input = request.getObjectId();
        input.getLogger().info(getContext().getObjectTypeId() + ":" + input.getObjectId());
        response.addEmptyResult(input, OperationStatus.SUCCESS, "404", "not found");
    }

}

// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.sdk.edu;

import com.boomi.connector.api.DeleteRequest;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.util.BaseDeleteOperation;

/**
 * 
 */
public class SampleDeleteOperation extends BaseDeleteOperation {

    protected SampleDeleteOperation(OperationContext context) {
        super(context);
    }

    @Override
    protected void executeDelete(DeleteRequest request, OperationResponse response) {   
        response.getLogger().info("Shared Field: " + getContext().getOperationProperties().getProperty("shared"));
        
        for (ObjectIdData input : request) {
            input.getLogger().info(getContext().getObjectTypeId() + ":" + input.getObjectId());
            response.addEmptyResult(input, OperationStatus.SUCCESS, "200", "deleted");
        }
    }

}

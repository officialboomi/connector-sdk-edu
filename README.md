# Connector SDK Education 

This project is intended to serve as a hands on introduction to the connector sdk. The project is composed of several
ordered modules that progressively build on concepts from the previous module. The general usage pattern would be to
modify the "getting started" (00) project as you move through the exercises for each module. If you get stuck on a
module but need to move on, you can switch to the "solution" for the uncompleted exercise so that you can continue with
subsequent exercies.  For example, if you're unable to complete module 03 but would like to move on to module 04, you
can switch into module 03 which contains all of the functionality that should have been completed in modules 00-03. 
While working through the course, students should not attempt to commit changes to the education repo. Instead, you can
fork or copy the repo. 

Exercises in this project do not actually "connect" to a live system as the focus is on the sdk components. Unless
otherwise noted, you should build and deploy the connector after each numbered exercise.

## Building the project  

This project uses gradle and requires java 8. 

To compile all modules: ```gradlew clean build```

To compile a single module: ```gradlew clean <project>:build```

## 00-getting-started 

This is the starting point for an education session. As you work through the exerices for each module, it's recomended
that you build on this project. If you get stuck at any point, you can switch to a new base project as described above.
There are a few placeholder files in this project. Specifically, a java class to represent the core _Connector_ class
and skeleton platform/runtime configuration files. 

## 01-connector-basics 

An introduction to building a connector. 

### Key Concepts 
* Platform Configuration (connector-descriptor.xml)
* Runtime Configuration (connector-config.xml)
* _Connector_
* _Operation_
* _Browser_ 
* _AtomContext_
* _BrowseContext_
* _OperationContext_ 
* _ConnectorException_ 
* Container Logs 

### Exercises 
Complete ALL numbered steps before deploying the connector 

1. Update _SampleConnector_ to implement the _Connector_ interface 
    * do nothing in _void_ methods  
    * throw an _UnsupportedOperationException_ in the other methods 
1. Create the runtime configuration 
    * connectorClassName should point to the fully qualified _SampleConnector_ class
    * sdkApiVersion should reference the latest available version 
1. Create the platform configuration
    * define an EXECUTE operation with supportsBrowse="false"        
1. Implement an _Operation_ 
    * create a new class that implements _Operation_
    * throw a _ConnectorException_ from the _execute_ method
1. Add a static _Logger_ to _SampleConnector_
1. Update _SampleConnector_ to create new instances of the operation and log a message 
1. Build and Deploy the connector 

## 02-operation-basics 

An introduction to connector operations with a focus on the "update" style operations (EXECUTE, CREATE, UPDATE, UPSERT).

### Key Concepts 
* _OperationRequest_
* _OperationResponse_
* _UpdateRequest_
* _TrackedData_
* _ObjectData_
* _Payload_
* _OperationStatus_ 
* _BaseUpdateOperation_
* Connection and Operation Properties 
* Document / Process Logs  

### Exercises 
1. Update the execute operation to not throw an exception (i.e., it won't do anything)
    * what happens when you execute the operation? 
1. Update the execute operation to iterate over the input documents but do not add any results 
    * cast _OperationRequest_ to _UpdateRequest_ and iterate over the _ObjectData_ instances  
    * what happens when you execute the operation? 
1. Add a result to the _OperationResponse_ for each input document (_ObjectData_)
    * use _addResult_ method to add a result 
        * _OperationStatus.SUCCESS_ 
        * any status code and message you like 
        * null _Payload_ 
1. Add 2 results for each input
    * call _addResult_ twice for each _ObjectData_ instance 
    * what happens when you execute the operation? 
1. Update the operation to extend _BaseUpdateOperation_ 
    * remove the 2nd result from the previous exercise (i.e., only add 1 result per document)
    * replace _execute_ with _executeUpdate_ 
1. Add connection / operation fields  
    * add a connection field with id="id" and type="string"
    * add an operation field with id="id" and type="string"
    * update the operation to log the connection field using the process logger (_OperationResponse.getLogger_)
    * update the operation to log the connection field using the document logger for each document (_ObjectData.getLogger_)
1. Add labels and help text 
    * update the descriptor to include a label attribute and helpText element for both "id" fields 
1. Add an optional _Payload_ to the result 
    * add a new operation field with id="includePayload" and type="boolean" 
    * add a label and help text for the new field 
    * update the operation to store the new as final member variable 
    * provide a default value of false when accessing the property (_PropertyMap.getBooleanProperty_)
    * update the _executeUpdate_ method to use _PayloadUtil.toPayload(new Date().toString())_ in the result if the member variable is true (null otherwise)
    * create final member variables for the "id" fields and use those in the log messages 

## 03-browse-basics 

An introduction to connector browsing with a focus on unstructured data. 

### Key Concepts 
* _Browser_ Implementation 
* _ObjectTypes_
* _ObjectType_ 
* _ObjectDefinitions_
* _ObjectDefinition_
* _ObjectDefinitionRole_
* _OperationType_ 
* _ContentType_
* Default Object Filter 
* Browse Only Operation Properties
* Object Type ID 

### Exercises 
1. Implement the _Browser_
    * create a new class that implements _Browser_
    * implement _getObjectTypes_
        * return an empty instance (_new ObjectTypes()_)
    * implement _getObjectDefinitions_
        * throw an _UnsupportedOperationException_
    * Update _SampleConnector_ to return the new browser class from _createBrowser_ 
    * Update the EXECUTE operation in the descriptor to support browse     
1. Create object types and definitions 
    * update _getObjectTypes_ to return 10 object types 
    * use id = "objectX" and label = "Object Type X" where X is the object number (i.e., "object1", "object2", etc)
    * update _getObjectDefinitions_ to return valid _ObjectDefinitions_ 
        * loop through the provided roles 
        * if the role is input, add a new _ObjectDefinition_ with inputType = _ContentType.BINARY_ and outputType = _ContentType.NONE_ 
        * if the role is output, add a new _ObjectDefinition_ with outputType = _ContentType.BINARY_ and inputType = _ContentType.NONE_ 
1. Add user configurable object count 
    * update the browser class to extend _BaseBrowser_ 
    * add a new browse only operation field to the descriptor
        * use id = "objectCount" and type = "integer"
        * add a label and help text for the field 
        * use scope = "browseOnly"
    * update _getObjectTypes_ to use the new field to determine the number of objects (use 10 as the default)
    * update the _Payload_ used by the execute operation to include the object type id 
        * e.g., _PayloadUtil.toPayload(getContext().getObjectTypeId() + ":" + new Date().toString())_ 


## 04-object-id-operations 

An introduction to connector operations that accept an "object id" as input (GET, DELETE). 

### Key Concepts 
* _GetRequest_
* _DeleteRequest_
* _ObjectIdData_
* _BaseGetOperation_
* _BaseDeleteOperation_
* Object Type ID vs Object ID 
* Empty Results 
* Multi-Operation Defintions (connector-descriptor.xml)
* Optional Roles 
* "Hard Coded" Profiles 
* Allowed Values 
* Default Values (descriptor)
* Radio Buttons 

### Exercises 
1. Implement GET operation 
    * create a new class that extends _BaseGetOperation_ 
    * implement _executeGet_ 
        * log the object type id and object id to the document logger 
        * add a result using _addEmptyResult_ 
            * _OperationStatus.SUCCESS_ 
            * include status code and message 
    * update _SampleConnector_ to extend _BaseConnector_ instead of implement _Connector_
        * move the _createOperation_ logic for the execute operation to _createExecuteOperation_
    * implement _createGetOperation_ in _SampleConnector_ and return the new class 
1. Implement DELETE operation 
    * create a new class that extends _BaseDeleteOperation_ 
    * implement _executeDelete_ 
        * log the object type id and object id for each document to the document logger    
        * add a result using _addEmptyResult_ 
            * _OperationStatus.SUCCESS_ 
            * include status code and message 
    * implement _createDeleteOperation_ in _SampleConnector_ and return the new class       
    * update the _getObjectDefinitions_ in the _Browser_ implementation 
        * use _ContentType.NONE_ as the output type for _OperationType.DELETE_ only   
        * continue to use _ContentType.BINARY_ as the output type for other operations with _ObjectDefinitionRole.OUTPUT_
        * recomend extracting methods for determing _ContentType_
            * _private ContentType getInputType(ObjectDefinitionRole role)_
            * _private ContentType getOutputType(ObjectDefinitionRole role)_
    * update the GET operation in the descriptor to include DELETE 
        * use the pipe delimited syntax for declaring multiple operations 
1. Add an operation field to both GET and DELETE 
    * define a new field in the shared operation definition in the descriptor 
        * use id = "shared" and type = "string"
        * include a label an help text 
        * define allowed values for the field (include labels)
    * update _executeGet_ and _executeDelete_ to log the new field using the process logger     
1. Implement Radio Buttons and a Default Value      
    * update the "shared" field to use displayType="radio"
    * update the "shared" to use a defaultValue

## 05-query-operations

An introduction to QUERY operations. 

### Key Concepts 
* _QueryRequest_
* _FilterData_
* _QueryFilter_ 
* _BaseQueryOperation_
* Filters 
    * Fields 
    * Operators 
* Structured _ObjectDefinitions_
* Packaged Resources 
* _Expression_ 
* _SimpleExpression_ 
* Partial Results 
* Application Errors 
* Field Selection 
* Sorting 
* _JSONUtil_
* _JsonPayloadUtil_

### Exercises 
Note: There are two new files introduced in this module to assist with the solution (People.java and person.json). If
you are still building on a previous module (e.g., 00), you will need to copy these files to your working project.

For example: 
```cp 05-query-operations/src/main/resources/person.json 00-getting-started/src/main/resources/.```
```cp 05-query-operations/src/main/java/com/boomi/connector/sdk/edu/People.java 00-getting-started/src/main/java/com/boomi/connector/sdk/edu/.```

1. Implement QUERY Operation 
    * Add QUERY operation to descriptor 
    * Implement _Operation_ 
        * use the appropriate base class for this operation
        * return empty result 
    * Implement _createQueryOperation_ in _SampleConnector_ 
    * what happens when perform an import for this operation? 
1. Implement queryFilter in descriptor
    * what happens when perform an import for this operation? 
1. Add an "equals" filter operator 
    * after upload, perform an import and execute the operation 
    * notice filter display bug (CON-1457)
1. Update _Browser_ implementation to support JSON 
    * only for the QUERY operation 
    * only for the OUTPUT role 
    * use _JSONUtil.newJsonDefinitionFromResource_ to load "schema/person.json"
    * throw a _ConnectorException_ if the resource can't be loaded 
        * _JSONUtil.newJsonDefinitionFromResource_ throws an _IOException_ that needs to be converted to a _ConnectorException_ 
    * configure a simple filter expression in the operation component 
1. Add a label for the filter operator (e.g., "Equals") to the descriptor
1. Implement filter handling 
    * support _SimpleExpression_ only (optional: use _Args_ to help)
    * assign the expression property and argument value to local variables 
    * use _People.queryByFirstName(value)_ to simulate a query with multiple results 
    * add a partial result for each "person"
        * hint: _JsonPayloadUtil.toPayload_
    * return an empty result if the filter property is not "firstName"
1. Add support for "age" filter 
    * use _People.queryByAge(value)_ (note: this simulates a query failure)
    * continue to support queryByFirstName and return an empty result if the filter property is not "age" or "firstName"
    * what happens when you execute this operation? 
1. Add partial result error handling 
    * add an additional partial result with the exception message if an exception occurs while adding partial results
1. Enable field selection, sorting, and filter grouping 
    * enable field selection in descriptor
    * enable "unbounded" sorting in descriptor    
    * add "ascending" and "descending" sort orders to the descriptor 
    * disable filter grouping in descriptor
1. Implement sort order and field selection 
    * use the variants of _People.query*_ that accept field selection and sorting 

## 06-listen-operations 

An introduction to LISTEN operations and managers 

### Key Concepts 
* _ConnectorContext_
* _ListenManager_
* _ListenOperation_
* _BaseListenManager_
* _BaseListenOperation_
* _Listener_ 
* _PayloadBatch_ 
* _BasePollingManager_
* _BasePollingOperation_
* _ExecutorService_ 

### Exercises 
1. Implement Managed LISTEN Operation 
    * create new class that implements _ListenManager_ (optional: use _BaseListenManager_)
    * create new class that implements _ListenOperation_ (use _BaseListenOperation_)
    * add a static _Logger_ to both classes and log a message when each starts and stops 
    * update _SampleConnector_ to implement _ListenConnector_ 
        * implement _createListenManager_ and _createListenOperation_ and return the new classes         
    * add a LISTEN operation to the descriptor
    * deploy some listen operations in platform and watch the logs 
1. Simulate slow start/stop
    * add a loop or sleep to delay the completion of the start / stop methods 
    * observe the impact to to other listeners 
    * remove the blocking code when done 
1. Submit payloads when listener starts 
    * submit a few payloads directly in the operatin start method using the _Listener_ 
1. Submit payloads using a batch 
    * update the listen operation to submit all of the payloads in a single _PayloadBatch_ 
1. Implement Polling Listener 
    * create new class that extends _BasePollingManager_ 
    * create new class that extends _BasePollingOperation_ 
    * add logging to the _doStart_ and _doStop_ methods 
    * use _People.queryByFirstName(value)_ to simulate a query in _doPoll()_ 
        * convert the _Iterable<JsonNode>_ to _Iterable<Payload>_ 
        * hint: add the items to a _List<Payload>_ or use _CollectionUtil.apply_ 
    * update _SampleConnector_ so that _createListenManager_ and _createListenOperation_ return the new classes 
    * update the _Browser_ implementation so that LISTEN uses the same _ObjectDefinition_ as QUERY 
    * note: the base polling classes don't use batches yet 

## 07-custom-operations

An introduction to custom operations 

### Key Concepts 
* Custom Operations 
* Custom Operation Type 
* Combined Results 

### Exercises
1. Create a custom operation 
    * add customTypeId = "firstOperation" to the existing execute operation 
    * what happens to the existing operation when you upload the connector? 
1. Create a second custom operation 
    * add customTypeLabel = "My First Operation" to the existing execute operation 
    * define a second EXECUTE operation with customTypeId = "combine" and customTypeLabel = "Combine Results"
    * create a new class that extends _BaseExecuteOperation_ 
    * implement _executeUpdate_ 
        * use _OperationResponse.addCombinedResult_ to produce a single output for all input documents 
        * include a status code and message with a null payload 
    * update _SampleConnector.createExecuteOperation_ 
        * if custom operation type is "firstOperation", return original execute operation 
        * if custom operation type is "combine", return new execute operation 
        * otherwise, _throw UnsupportedOperationException_
            * note: this is not preserving backwards compatibility

## 08-document-properties

An introduction to the various types of document properties 

### Key Concepts 
* Dynamic Document Properties 
* Connector Document Properties 
* Connector Tracked Properties 
* _PayloadMetadataFactory_
* _PayloadMetadata_ 
* _ExtendedPayload_ 

### Exercises
1. Output dynamic document properties 
    * update the "firstOperation" operation to include a payload containing all dynamic document properties instead of the date string
2. Accept a connector document property as input 
    * define a dynamicProperty in the descriptor 
        * id = "inputProp", label = "Input Property", type = "string"
    * update the "firstOperation" to log this property using the docuemnt logger for each input         
3. Include a connector tracked property as payload metadata 
    * define a trackedProperty in the descriptor
        * id = "outputProp", label = "Output Property" (note: no type)
    * update the "firstOperation" to include _PayloadMeta_ in an _ExtendedPayload_ when including a _Payload_
        * create the _PayloadMetadata_ using either the _OperationContext_ or the _OperationResponse_ 
        * set the value to the current date 


## Contribution guidelines 

* TBD

